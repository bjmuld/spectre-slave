#!/bin/bash 

#PBS -o ~/manual-jobs/spectre-slave/
#PBS -l nodes=1:ppn=1,walltime=30:00
#PBS -N spectre-slave
#PBS -j oe

set -e -v

#module load python/2.7
module load git/2.13.4

source ~/miniconda2/etc/profile.d/conda.sh
conda activate spectre-slave

pip install -U pip

cd ~/manual-jobs/spectre-slave
git pull

pushd waverunner
git pull
popd

pushd pyspectre
git pull
popd

pip install -U -e ./waverunner
pip install -U -e ./pyspectre[server]

pyspectre_server -r 143.215.156.66:11234 -i 10

exit 0
