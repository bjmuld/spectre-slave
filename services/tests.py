#!/usr/bin/env python

from time import sleep

def sleep5s(phrase="you didn't give me anything to say"):
    sleep(5)
    return phrase
    
def sleep10s(phrase="you didn't give me anything to say"):
    sleep(10)
    return phrase
    
def sleepArg(sleeptime, phrase="you didn't give me anything to say"):
    sleep(sleeptime)
    return phrase
